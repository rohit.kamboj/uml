<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Privacy Policy - U&M Law Offices</title>
    <link rel="icon" href="img/favicon.ico">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="css/owl-carousel/owl.theme.css">
 
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">
    
     <!-- jquery -->
    <script src="js/jquery-1.12.1.min.js"></script>
    
    <script src="https://use.fontawesome.com/1a0fb0c8e3.js"></script>

</head>

<body>
    <!--::header part start::-->

    <section class="home">
        <header class="main_menu">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-12">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <a class="navbar-brand" href="http://krescentglobal.in/umlawoffices/"><img src="img/umlogo.png" alt="logo"></a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="http://krescentglobal.in/umlawoffices/">Home </a>
                                    </li>
                                    <!--li class="nav-item">
                                        <a class="nav-link" href="http://krescentglobal.in/umlawoffices/#about-us">About Us</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="http://krescentglobal.in/umlawoffices/#contact_us">Contact Us</a->
                                    </li-->
                                    <li class="nav-item">
                                        <a class="nav-link" href="http://krescentglobal.in/umlawoffices/careers.php">Careers</a>
                                    </li> 
                                    <li class="nav-item">
                                        <a class="nav-link" href="http://krescentglobal.in/umlawoffices/disclaimer.php">Disclaimer</a>
                                    </li> 
                                    <li class="nav-item active">
                                        <a class="nav-link" href="http://krescentglobal.in/umlawoffices/privacy-policy.php">Privacy Policy</a>
                                    </li>                       
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
    </section>

    <div class="container">
            <section class="service_part" id="privacy_policy">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div>
                        <h2>Privacy Policy</h2>                  
                    </div>
                </div>        
                <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="item" style="margin-bottom: 20px;">
                    <div class="single_service_part">
                        <h5>1. General</h5>
                        <p>U&M Law Offices (UML) is committed to protecting the privacy of personal information that has been provided to us and processes data in accordance with data protection regulations as applicable to you and us. Our Privacy Policy is designed and developed to ensure the privacy and security of your personal information provided to us. For the purpose of this policy, sensitive personal data or information has been considered as a part of personal information. This statement explains how we may collect and process information about:</p>
                        <ul>
                            <li>Visitors to our website</li>
                            <li>People who use our services</li>
                            <li>Job applicants</li>
                            <li>Interns</li>
                            <li>UML consultants (retained lawyers)</li>
                            <li>Current employees</li>
                        </ul>

                        <p style="margin-top:20px;">This Privacy Policy is designed to make you understand:</p>
                        <ul>
                            <li>Personal information that we collect and methods of collection</li>
                            <li>Purpose of collecting personal information</li>
                            <li>Measures taken by us to ensure safety and privacy of your personal information</li>
                            <li>Ways by which you can access and change your personal data</li>
                        </ul>
                        &nbsp;
                        &nbsp;
                        <h5>2. Collection of Personal Information</h5>
                        <p>"Personal information" is any information that can be used to uniquely identify a person. We may collect your personal information as required under laws and regulations and when you are a recipient of our services, apply for a position with us, or are employed or retained by us. The information we collect about you will depend on your relationship with us. We may also collect your personal data from other sources, including sources in the public domain. The information we may collect includes, but is not limited to, the following:</p>
                        <ul>
                            <li>Your name, your employer and your title, postal address, phone number, date of birth and email address.</li>
                            <li>Details of your visits to any of our offices, including meeting dates and times.</li>
                            <li>Data related to your visit of our website or electronic mailers and updates that we may send to you from time to time.</li>
                            <li>In the case of employees, UML consultants (retained lawyers) and interns: certain financial information such as your bank account details and biometric information such as fingerprints.</li>
                            <li>Your contact with us, such as: an email or letter you send to us and other records of any contact you may have with us.</li>
                            <li>Your account information, such as: dates of payments made, owed or received.</li>
                        </ul>
                        &nbsp;
                        &nbsp;
                        <h5>3. How we use this information</h5>
                        <p>Unless required by applicable data protection regulation, UML will not disclose, use, give or distribute any personal information to third parties for any purposes other than to those third parties that need to know, and such need to know is necessary for delivery of services in relation to your engagement or employment with us.</p>

                        <p>We may disclose personal information to third parties where:</p>
                        <ul>
                            <li>You have consented to us sharing your personal information in this way.</li>
                            <li>We are under a legal, regulatory or professional obligation to do so (for example, in order to comply with applicable laws) or to protect the rights and interests, property, or safety of UML, our clients or others.</li>
                            <li>It is relevant in the circumstances to disclose the information to our clients, your employer or place of business, and your professional advisers.</li>
                            <li>We use a third party service provider to provide services that involve data processing, for example archival, auditing, professional advisory (including legal, accounting, financial and business consulting), technology, website, research, banking, payment, client contact, data processing, insurance, forensic, litigation support, and security services.</li>
                        </ul>

                        <p>Some of the third parties with whom we share personal information may be located outside your country or the country from which the personal information was provided. While such third parties will often be subject to privacy and confidentiality obligations, you accept that, where lawful, such obligations may differ from and be less stringent than the requirements of the privacy laws of the jurisdiction of origin.</p>
                        &nbsp;
                        &nbsp;
                        <h5>4. How we protect your personal information</h5>
                        <p>UML is committed to protecting the information we receive from you. We take all appropriate, industry standard security measures possible to protect your information against misuse, unauthorised access to or unauthorised alteration, disclosure or destruction of data. To prevent unauthorised access, maintain data accuracy, and ensure the correct use of information, we maintain appropriate physical, electronic, and managerial procedures to safeguard and secure the information and data stored on our system. While no computer system is completely secure, we believe the measures we have implemented reduce the likelihood of security problems to a level appropriate to the type of data involved.</p>
                        &nbsp;
                        &nbsp;
                        <h5>5. Your rights regarding your information</h5>
                        <p>You have the following rights with regard to your Personal Information:</p>
                        <ul>
                        <li>You may request the removal of your personal information from UML’s systems at any time by emailing the address provided below. If a request for removal is made, we will exercise all reasonable efforts to promptly remove your personal information. However, we do not guarantee the permanent removal of your personal information, which may, for example, remain in archival or backup copies. UML employees, interns and consultants should note that such a request may impact your employment or engagement with UML.</li>
                        <li>You may request the removal of your personal information from UML’s systems at any time by emailing the address provided below. If a request for removal is made, we will exercise all reasonable efforts to promptly remove your personal information. However, we do not guarantee the permanent removal of your personal information, which may, for example, remain in archival or backup copies. UML employees, interns and consultants should note that such a request may impact your employment or engagement with UML.</li>
                        </ul>
                        &nbsp;
                        &nbsp;
                        <h5>6. Status of this policy</h5>
                        <p>Technologies and information governance practices are constantly developing. We may therefore need to revise this Privacy Policy in the future. You should therefore review this policy regularly to ensure that you are aware of any changes to its terms.</p>
                        &nbsp;
                        &nbsp;
                        <h5>7. Contacts</h5>
                        <p>If you have any questions about this Privacy Policy, want to exercise your right to see a copy of the information that we hold about you, or think that information we hold about you may need to be corrected/ updated, want to delete all or any part of it or to object to the processing on legitimate grounds, please contact us with enquiry topic "Data privacy" at <a href="mailto:helpdesk@umlawoffices.com">helpdesk@umlawoffices.com</a>.</p>
                    </div>            
                  </div>
                </div>
            </div>
            <div class="hero-app-3 mobile-hide"><img src="img/dot_bg .png" alt=""></div>
        </div>
    </section>
    </div>

<section class="footer-area section_padding" id="contact_us">
      <div class="container-fluid">
      <!--Section heading-->
      <h2 class="h1-responsive font-weight-bold text-center my-4">Contact us</h2>
      <!--Section description-->
      <p class="text-center w-responsive mx-auto mb-5">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
      a matter of hours to help you.</p>

      <div class="row">
      <!--Grid column-->
      <div class="col-md-9 mb-md-0 mb-5">
      <form id="contact-form" name="contact-form" method="POST">
      <div id="mail-status"></div>
      <!--Grid row-->
      <div class="row">
        <div class="col-md-4">
          <div class="md-form mb-0">
            <label for="name" class="">Name *</label>
            <span id="name-info" class="info"></span>
            <input type="text" id="name" name="name" class="form-control">            
          </div>
        </div>

        <div class="col-md-4">
          <div class="md-form mb-0">
            <label for="email" class="">Email Address *</label>
            <span id="email-info" class="info"></span>
            <input type="text" id="email" name="email" class="form-control">
          </div>
        </div>

        <div class="col-md-4">
          <div class="md-form mb-0">
            <label for="phone" class="">Phone *</label>
            <span id="phone-info" class="info"></span>
            <input type="text" id="phone" name="phone" class="form-control">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="md-form mb-0">
            <label for="subject" class="">Subject *</label>
            <span id="subject-info" class="info"></span>
            <input type="text" id="subject" name="subject" class="form-control">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="md-form">
            <label for="message">Message *</label>
            <span id="message-info" class="info"></span>
            <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
          </div>
        </div>
      </div>
      </form>

      <div class="text-center text-md-left contact">
        <button class="btn btn-primary send_contact_mail">Submit</button>
        <span style="float:right;color:#ffffff;">* indicates required field.</span>
      </div>
      <div class="status"></div>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center">
          <ul class="list-unstyled mb-0">
            <!--li><i class="fa fa-phone mt-4 fa-2x"></i>
              <p>+ 01 234 567 89</p>
            </li-->

            <li><i class="fa fa-envelope mt-4 fa-2x"></i>
              <p><a href="mailto:info@umlawoffices.com">info@umlawoffices.com</a></p>
            </li>
          </ul>
        </div>
        <!--Grid column-->
      </div>
      <!--Section: Contact v.2-->
    </div>
    </section>
    <!--::start_journey end::-->

    <!-- footer part start-->
    <section class="footer-area section_padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-2 col-md-2 col-sm-12 mb-4 mb-xl-0 single-footer-widget">
                    <a class="footer_logo" href="http://krescentglobal.in/umlawoffices/"><img src="img/umlogo.png" alt="logo"></a>
                </div>
                <div class="col-xl-7 col-md-7 col-sm-12 mb-4 mb-xl-0 single-footer-widget alignB text-center footer-menu">
                    <ul>
                        <li class="nav-item"><a class="footer_menu_links" href="http://krescentglobal.in/umlawoffices/">Home</a></li> | 
                        <li class="nav-item active"><a class="footer_menu_links" href="http://krescentglobal.in/umlawoffices/careers.php">Careers</a></li> | 
                        <li class="nav-item"><a class="footer_menu_links" href="http://krescentglobal.in/umlawoffices/disclaimer.php">Disclaimer</a></li> |
                        <li class="nav-item"><a class="footer_menu_links" href="http://krescentglobal.in/umlawoffices/privacy-policy.php">Privacy policy</a></li> 
                        
                        <!--li class="nav-item"><a class="footer_menu_links" href="http://krescentglobal.in/umlawoffices/#contact_us">Contact Us</a></li-->
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 text-center footer-social alignB">
                    <h4>Follow Us</h4>
                    <a target="_blank" href="https://www.facebook.com/umlawoffices"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                    <a target="_blank" href="https://twitter.com/UMLawOffices"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                    <a target="_blank" href="https://www.instagram.com/UMLawOffices_/?hl=en"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a target="_blank" href="https://www.linkedin.com/company/umlawoffices"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </section>  
    
    <footer class="copyright_part">
        <div class="container-fluid">
            <div class="row text-center">
                <p class="footer-text m-0 col-lg-12">Copyright &copy;2020 <a href="#home">U&M Law Offices</a> | All Rights Reserved.</p>                
            </div>
        </div>
    </footer>
    <!-- footer part end-->
    
    <script>
        jQuery(document).ready(function() 
        { 
          jQuery("#owl-demo").owlCarousel({      
            items : 4,
            itemsDesktop : [1199,4],
            itemsDesktopSmall : [980,3],
            itemsTablet: [768,2],
            itemsTabletSmall: false,
            itemsMobile : [479,1],
            singleItem : false,
         
            //Basic Speeds
            slideSpeed : 100,
            paginationSpeed : 800,
            rewindSpeed : 1000,
         
            //Autoplay
            autoPlay : true,
            stopOnHover : true,
         
            // Navigation
            pagination :  false,
            navigation : true,
            navigationText : ["<",">"],
            rewindNav : true,
            scrollPerPage : false,
            
            // Responsive 
            responsive: true,
            responsiveRefreshRate : 200,
            responsiveBaseWidth: window,
            
            // CSS Styles
            baseClass : "owl-carousel",
            theme : "owl-theme",
          });  

    $(document).on('click','.send_contact_mail',function() 
    {
        var valid;  
        valid = validateContact();
        if(valid) {
            jQuery.ajax({
                url: "contact_mail.php",
                data:'name='+$("#name").val()+'&email='+
                $("#email").val()+'&phone='+
                $("#phone").val()+'&subject='+
                $("#subject").val()+'&message='+
                $(message).val(),
                type: "POST",
                success:function(data){
                    $("#mail-status").html(data);
                    $("#contact-form").trigger("reset");
                },
                error:function (){}
            });
        }
    });

    function validateContact() 
    {
        var valid = true;   
        $(".demoInputBox").css('border-color','');
        $("#name").css('border-color','');
        $("#email").css('border-color','');
        $("#phone").css('border-color','');
        $("#subject").css('border-color','');
        $("#message").css('border-color','');
        $(".info").html('');
        if(!$("#name").val()) 
        {
            $("#name-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#name").css('border-color','#ff0000');
            valid = false;
        }
        if(!$("#email").val()) 
        {
            $("#email-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#email").css('border-color','#ff0000');
            valid = false;
        }
        if(!$("#phone").val()) 
        {
            $("#phone-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#phone").css('border-color','#ff0000');
            valid = false;
        }
        if(!$("#email").val().match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) 
        {
            $("#email-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#email").css('border-color','#ff0000');
            valid = false;
        }
        if(!$("#subject").val()) 
        {
            $("#subject-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#subject").css('border-color','#ff0000');
            valid = false;
        }
        if(!$("#message").val()) 
        {
            $("#message-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#message").css('border-color','#ff0000');
            valid = false;
        }
        return valid;
    }       
        });
    </script>
    

    <!-- jquery plugins here-->

    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- particles js -->
    <script src="js/owl-carousel/owl.carousel.js"></script>
     <!-- easing js -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- custom js -->
    <script src="js/custom.js"></script>
</body>

</html>