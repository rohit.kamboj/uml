jQuery('document').ready(function()
{
	$.getJSON("http://ipinfo.io", function (ipdata) 
    {
        console.log("City: " + ipdata.city + " ,County: " + ipdata.country + " ,IP: " + ipdata.ip + " ,Location: " + ipdata.loc + " ,Organisation: " + ipdata.org + " ,Postal Code: " + ipdata.postal + " ,Region: " + ipdata.region + "");
        jQuery('input[name="ip_address"]').val(ipdata.ip);
        jQuery('input[name="ip_region"]').val(ipdata.region);
        jQuery('input[name="ip_country"]').val(ipdata.country);
    });

    /*****************************************************************************************************/
    (function ($) 
    {
      "use strict";
        // menu fixed js code
        $(window).scroll(function () 
        {
            var window_top = $(window).scrollTop() + 1;
            if (window_top > 50) 
            {
                $('.home .main_menu').addClass('menu_fixed animated fadeInDown');
            } 
            else 
            {
                $('.home .main_menu').removeClass('menu_fixed animated fadeInDown');
            }
        });
    }(jQuery));
});