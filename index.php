<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>U&M Law Offices</title>
    <link rel="icon" href="img/favicon.ico">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="css/owl-carousel/owl.theme.css">
 
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">
	
	 <!-- jquery -->
    <script src="js/jquery-1.12.1.min.js"></script>
	
	<script src="https://use.fontawesome.com/1a0fb0c8e3.js"></script>

</head>

<body>
    <!--::header part start::-->
<section class="home">
    <header class="main_menu">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="http://krescentglobal.in/umlawoffices/"> <img src="img/umlogo.png" alt="logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <li class="nav-item active">
                                    <a class="nav-link" href="http://krescentglobal.in/umlawoffices/">Home </a>
                                </li>
                                <!--li class="nav-item">
                                    <a class="nav-link" href="#about-us">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#contact_us">Contact Us</a>
                                </li-->
                                <li class="nav-item">
                                        <a class="nav-link" href="http://krescentglobal.in/umlawoffices/careers.php">Careers</a>
                                    </li> 
                                <li class="nav-item">
                                    <a class="nav-link" href="http://krescentglobal.in/umlawoffices/disclaimer.php">Disclaimer</a>
                                </li> 
                                <li class="nav-item">
                                    <a class="nav-link" href="http://krescentglobal.in/umlawoffices/privacy-policy.php">Privacy Policy</a>
                                </li>                       
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
</section>

    <section id="home" class="banner_part">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-7 col-xl-6">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <h1>One of the Leading Law Firm</h1>
                            <p>U & M Law Offices (UML) is one of the leading national law firms in India. We focus on result based efforts and implement comprehensive services. We are a team of experienced Professionals with more than 25 years of experience in domain. We are situated at Panchkula-(Haryana).</p>
                            <p>If you would like to have any additional information, please contact us at: <a href="mailto:info@umlawoffices.com">info@umlawoffices.com</a></p>
                            &nbsp;
                            &nbsp;
                            <span style="font-size:14px;font-weight: 700;display:block;line-height:1.6;">By visiting this site, you acknowledge that you have entered this site to gather general information about U & M Law Offices. </span>
                            <a href="#our_products" class="btn_1">Learn More </a>
						                <a href="#contact_us" class="btn_4">Get in Touch </a>                    
                        </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->

    <section class="service_part" id="our_products">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div>
                        <h2>Area of Practices</h2>                  
                    </div>
                </div>        
                <div class="col-sm-4 col-md-4 col-lg-4">
                  <div class="item" style="margin-bottom: 20px;">
                    <div class="single_service_part">
                      <ul>
                        <li>Agriculture and Forestry</li>
                        <li>Capital Markets</li>
                        <li>Construction & Engineering</li>
                        <li>Defence & Internal Security</li>
                        <li>Education</li>
                        <li>Energy: Power & Hydrocarbons</li>
                        <li>Environment</li>
                        <li>Financial Services & Insolvency</li>
                      </ul>
                    </div>            
                  </div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4">
                  <div class="item" style="margin-bottom: 20px;">
                    <div class="single_service_part">
                      <ul>
                        <li>Hospitality, Tourism & Retail</li>
                        <li>Insurance & Pension</li>
                        <li>Knowledge Based Industries</li>
                        <li>Manufacturing</li>
                        <li>Media and Sports</li>
                        <li>Mines & Minerals</li>
                        <li>Non-Governmental Sector</li>
                        <li>Pharma</li>
                      </ul>
                    </div>            
                  </div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4">
                  <div class="item" style="margin-bottom: 20px;">
                    <div class="single_service_part">
                      <ul>
                        <li>Real Estate</li>
                        <li>Services</li>
                        <li>Smart Cities</li>
                        <li>Startups</li>
                        <li>Telecommunications</li>
                        <li>Media & Technology</li>
                        <li>Transportation</li>
                        <li>Logistics</li>
                      </ul>
                    </div>            
                  </div>
                </div>
            </div>
            <div class="hero-app-3 mobile-hide"><img src="img/dot_bg .png" alt=""></div>
        </div>
    </section>

    <!-- agent_profile part start-->
    <section id="about-us" class="about_part padding_top">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="section_tittle text-center">
                        <h2>What make us different?</h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-end">
              <div class="col-md-5 col-xl-4">
                <div class="about_img">
                  <img src="img/agent_pic.png" alt="">
                </div>
              </div>
              <div class="col-md-7 col-xl-8">
                <div class="about_text">
                  <ul>
                    <li>Our Team of  Expert Professionals</li>
                    <li>Our Work Culture, Attitudes and Aspirations</li>
                    <li>Result Centric Approach and Solutions</li>
                    <li>Technical Background and usage of technology</li>
                    <li>Global Reach</li>
                    <li>Our Preferred Partners</li>
                    <li>Consistent focus on Quality</li>
                    <li>Discussions & knowledge sharing</li>
                  </ul>
                </div>
              </div>
            </div>
        </div>
        <div class="hero-app-1 mobile-hide"><img src="img/dot_bg .png" alt=""></div>
        <div class="hero-app-2 mobile-hide"><img src="img/dot_bg .png" alt=""></div>
    </section>
  
    <!-- start_journey part start-->
    <section class="journey blue-bg padding_top">
        <div class="container-fluid">
              <div class="row justify-content-center">
                  <div class="col-xl-12">
                      <div class="text-center">
                          <h2 class="whiteColor">50+ Qualified Professionals with domain knowledge are <br>ever ready to provide support!!</span></h2>
                      </div>
                  </div>
              </div>
              <div class="row text-center">
                  <div class="col-xl-1 mobile-hide">
                  </div>
                  <div class="col-md-12 col-xl-10">
                      <div>
                          <p class="whiteColor">Technical enabled service deliverables, management system for document and knowledge based platform to leverage industry best practices!!</p>
                          <p class="whiteColor">Network of International Relationships for seamless cross-border global support!!</p>
                          <!--h2 class="whiteColor"><a target="_blank" href="#" class="btn_1">Get Started</a></h2-->
                      </div>
                  </div>
                  <div class="col-xl-1 mobile-hide">
                  </div>
              </div>
        </div>
        <div class="hero-app-1 mobile-hide"><img src="img/dot_bg .png" alt="" /></div>
        <div class="hero-app-2 mobile-hide"><img src="img/dot_bg .png" alt="" /></div>
    </section>

    <section class="footer-area section_padding" id="contact_us">
      <div class="container-fluid">
      <!--Section heading-->
      <h2 class="h1-responsive font-weight-bold text-center my-4">Contact us</h2>
      <!--Section description-->
      <p class="text-center w-responsive mx-auto mb-5">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
      a matter of hours to help you.</p>

      <div class="row">
      <!--Grid column-->
      <div class="col-md-9 mb-md-0 mb-5">
      <form id="contact-form" name="contact-form" method="POST">
      <div id="mail-status"></div>
      <!--Grid row-->
      <div class="row">
        <div class="col-md-4">
          <div class="md-form mb-0">
            <label for="name" class="">Name *</label>
            <span id="name-info" class="info"></span>
            <input type="text" id="name" name="name" class="form-control">            
          </div>
        </div>

        <div class="col-md-4">
          <div class="md-form mb-0">
            <label for="email" class="">Email Address *</label>
            <span id="email-info" class="info"></span>
            <input type="text" id="email" name="email" class="form-control">
          </div>
        </div>

        <div class="col-md-4">
          <div class="md-form mb-0">
            <label for="phone" class="">Phone *</label>
            <span id="phone-info" class="info"></span>
            <input type="text" id="phone" name="phone" class="form-control">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="md-form mb-0">
            <label for="subject" class="">Subject *</label>
            <span id="subject-info" class="info"></span>
            <input type="text" id="subject" name="subject" class="form-control">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="md-form">
            <label for="message">Message *</label>
            <span id="message-info" class="info"></span>
            <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
          </div>
        </div>
      </div>
      </form>

      <div class="text-center text-md-left contact">
        <button class="btn btn-primary send_contact_mail">Submit</button>
        <span style="float:right;color:#ffffff;">* indicates required field.</span>
      </div>
      <div class="status"></div>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center">
          <ul class="list-unstyled mb-0">
            <!--li><i class="fa fa-phone mt-4 fa-2x"></i>
              <p>+ 01 234 567 89</p>
            </li-->

            <li><i class="fa fa-envelope mt-4 fa-2x"></i>
              <p><a href="mailto:info@umlawoffices.com">info@umlawoffices.com</a></p>
            </li>
          </ul>
        </div>
        <!--Grid column-->
      </div>
      <!--Section: Contact v.2-->
    </div>

    </section>
    <!--::start_journey end::-->

    <!-- footer part start-->
    <section class="footer-area section_padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-2 col-md-2 col-sm-12 mb-4 mb-xl-0 single-footer-widget">
                    <a class="footer_logo" href="http://krescentglobal.in/umlawoffices/"><img src="img/umlogo.png" alt="logo"> </a>
                </div>
                <div class="col-xl-7 col-md-7 col-sm-12 mb-4 mb-xl-0 single-footer-widget alignB text-center footer-menu">
                    <ul>
                        <li class="nav-item active"><a class="footer_menu_links" href="http://krescentglobal.in/umlawoffices">Home</a></li> | 
                        <li class="nav-item active"><a class="footer_menu_links" href="http://krescentglobal.in/umlawoffices/careers.php">Careers</a></li> | 
                        <li class="nav-item"><a class="footer_menu_links" href="http://krescentglobal.in/umlawoffices/privacy-policy.php">Privacy policy</a></li> |
                        <li class="nav-item"><a class="footer_menu_links" href="http://krescentglobal.in/umlawoffices/disclaimer.php">Disclaimer</a></li>
                        <!--li class="nav-item"><a class="footer_menu_links" href="http://krescentglobal.in/umlawoffices/#contact_us">Contact Us</a></li-->
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 text-center footer-social alignB">
                    <h4>Follow Us</h4>
                    <a target="_blank" href="https://www.facebook.com/umlawoffices"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                    <a target="_blank" href="https://twitter.com/UMLawOffices"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                    <a target="_blank" href="https://www.instagram.com/UMLawOffices_/?hl=en"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a target="_blank" href="https://www.linkedin.com/company/umlawoffices"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </section>

    <footer class="copyright_part">
        <div class="container-fluid">
            <div class="row text-center">
                <p class="footer-text m-0 col-lg-12">Copyright &copy;2020 <a href="#home">U&M Law Offices</a> | All Rights Reserved.</p>                
            </div>
        </div>
    </footer>
    <!-- footer part end-->
	
	<script>
		jQuery(document).ready(function() 
		{ 
		  jQuery("#owl-demo").owlCarousel({		 
			items : 4,
			itemsDesktop : [1199,4],
			itemsDesktopSmall : [980,3],
			itemsTablet: [768,2],
			itemsTabletSmall: false,
			itemsMobile : [479,1],
			singleItem : false,
		 
			//Basic Speeds
			slideSpeed : 100,
			paginationSpeed : 800,
			rewindSpeed : 1000,
		 
			//Autoplay
			autoPlay : true,
			stopOnHover : true,
		 
			// Navigation
			pagination :  false,
			navigation : true,
			navigationText : ["<",">"],
			rewindNav : true,
			scrollPerPage : false,
			
			// Responsive 
			responsive: true,
			responsiveRefreshRate : 200,
			responsiveBaseWidth: window,
			
			// CSS Styles
			baseClass : "owl-carousel",
			theme : "owl-theme",
		  });	

    $(document).on('click','.send_contact_mail',function() 
    {
        var valid;  
        valid = validateContact();
        if(valid) {
            jQuery.ajax({
                url: "contact_mail.php",
                data:'name='+$("#name").val()+'&email='+
                $("#email").val()+'&phone='+
                $("#phone").val()+'&subject='+
                $("#subject").val()+'&message='+
                $(message).val(),
                type: "POST",
                success:function(data)
                {
                    $("#mail-status").html(data);
                    $("#contact-form").trigger("reset");
                },
                error:function (){}
            });
        }
    });

    function validateContact() 
    {
        var valid = true;   
        $(".demoInputBox").css('border-color','');
        $("#name").css('border-color','');
        $("#email").css('border-color','');
        $("#phone").css('border-color','');
        $("#subject").css('border-color','');
        $("#message").css('border-color','');
        $(".info").html('');
        if(!$("#name").val()) 
        {
            $("#name-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#name").css('border-color','#ff0000');
            valid = false;
        }
        if(!$("#email").val()) 
        {
            $("#email-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#email").css('border-color','#ff0000');
            valid = false;
        }
        if(!$("#phone").val()) 
        {
            $("#phone-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#phone").css('border-color','#ff0000');
            valid = false;
        }
        if(!$("#email").val().match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) 
        {
            $("#email-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#email").css('border-color','#ff0000');
            valid = false;
        }
        if(!$("#subject").val()) 
        {
            $("#subject-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#subject").css('border-color','#ff0000');
            valid = false;
        }
        if(!$("#message").val()) 
        {
            $("#message-info").html("<span style='color:#ff0000;'>(Required)</span>");
            $("#message").css('border-color','#ff0000');
            valid = false;
        }
        return valid;
    } 

		});
	</script>
	

    <!-- jquery plugins here-->

    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- particles js -->
    <script src="js/owl-carousel/owl.carousel.js"></script>
     <!-- easing js -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- custom js -->
    <script src="js/custom.js"></script>
</body>

</html>